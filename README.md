

```bash
java -jar target/demo-0.0.1-SNAPSHOT.jar --spring.config.location=/home/szhao/Downloads/application.properties,classpath:/application.properties
```

configurations in external will be overridden by `src/main/resources/application.properties`

```bash
java -jar target/demo-0.0.1-SNAPSHOT.jar --spring.config.location=/home/szhao/Downloads/application.properties
```
 missing configurations ==> application fail to startup 
 
 
But Laurent said (REASON: if we use 1.5.14.RELEASE, it will be following behavior)

java -jar target/demo-0.0.1-SNAPSHOT.jar --spring.config.location=/home/szhao/Downloads/application.properties 

=> if file exist AND a key i NOT in this file, but in the "application.properties" embedded in the JAR 

=> it use the JAR one

#### Configuration Location
(from Spring Boot 2.0 Migration Guide)

The behavior of the spring.config.location configuration has been fixed; it previously added a location to the list of default ones, now it replaces the default locations. If you were relying on the way it was handled previously, you should now use spring.config.additional-location instead.