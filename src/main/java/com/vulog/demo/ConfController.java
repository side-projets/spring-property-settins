package com.vulog.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfController {

  @Value("${business.url}")
  private String BUSINESS_URL;

  @Value("${invoice.url}")
  private String INVOICE_URL;

  private static final Logger LOGGER = LoggerFactory.getLogger(ConfController.class);

  @GetMapping("/properties")
  public ResponseEntity getProperties() {
    String s = String.format("BUSINESS %s INVOICE %s", BUSINESS_URL, INVOICE_URL);
    LOGGER.info(s);
    return ResponseEntity.ok(s);
  }
}
