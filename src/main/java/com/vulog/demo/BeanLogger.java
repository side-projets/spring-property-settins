package com.vulog.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
public class BeanLogger {
  private static final Logger LOGGER = LoggerFactory.getLogger(BeanLogger.class);

  @EventListener
  public void logBeans(ContextRefreshedEvent event) {
    String[] beans = event.getApplicationContext().getBeanDefinitionNames();
    LOGGER.info("=================== Beans in App ====================");
    Stream.of(beans).sorted().forEach(LOGGER::info);
  }
}
